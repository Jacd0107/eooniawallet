import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wallet/config.dart';
import 'package:wallet/constant.dart';
import 'package:wallet/core/bloc/wallet-bloc/cubit/wallet_cubit.dart';
import 'package:wallet/ui/home/home_screen.dart';
import 'package:wallet/ui/onboard/create_wallet_screen.dart';
import 'package:wallet/ui/shared/wallet_button.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController passwordController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey();
  @override
  Widget build(BuildContext context) {
    return BlocListener<WalletCubit, WalletState>(
      listener: (context, state) {
        if (state is WalletUnlocked) {
          Navigator.of(context).popUntil((route) => route.isFirst);
          Navigator.of(context).pushReplacementNamed(HomeScreen.route,
              arguments: {"password": passwordController.text});
          // Navigator.of(context).pushNamed(HomeScreen.route, arguments: {"password": passwordController.text});
        }
        if (state is WalletErased) {
          Navigator.of(context).pushNamed(CreateWalletScreen.route);
        }
      },
      child: Scaffold(
        body: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const SizedBox(
                height: 10,
              ),
              const Expanded(child: SizedBox()),
              Center(
                child: Text(
                  appName.toUpperCase(),
                  style: const TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w300,
                      fontSize: 25,
                      letterSpacing: 5),
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: const Text(
                    "Welcome Back!",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
                  )),
              const SizedBox(
                height: 30,
              ),
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: Text("Password"),
              ),
              const SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: TextFormField(
                  controller: passwordController,
                  validator: (String? string) {
                    if (string!.isEmpty) {
                      return "Password shouldn't be empty";
                    }
                    return null;
                  },
                  decoration: const InputDecoration(
                      hintText: "Enter password to unlock the wallet",
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey)),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: kPrimaryColor)),
                      errorBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: kPrimaryColor)),
                      border: OutlineInputBorder(borderSide: BorderSide())),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              WalletButton(
                  type: WalletButtonType.outline,
                  textContent: "Open Wallet",
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      context.read<WalletCubit>().initialize(
                            passwordController.text,
                            onError: ((p0) => {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      backgroundColor: Colors.red,
                                      content: Row(
                                        children: [
                                          const Icon(
                                            Icons.error,
                                            color: Colors.white,
                                          ),
                                          const SizedBox(
                                            width: 10,
                                          ),
                                          SizedBox(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  1.30,
                                              child: const Text(
                                                  "Password incorrect, provider valid password"))
                                        ],
                                      ),
                                    ),
                                  )
                                }),
                          );
                    }
                  }),
              const Expanded(child: SizedBox()),
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: Text(
                  "Can't login? You can EARSE your current wallet and",
                  textAlign: TextAlign.center,
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: InkWell(
                  onTap: () {
                    var alert = AlertDialog(
                        actions: [
                          ElevatedButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all(kPrimaryColor)),
                            child: const Text("Cancel"),
                          ),
                          ElevatedButton(
                              onPressed: () {
                                context.read<WalletCubit>().eraseWallet();
                              },
                              style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all(Colors.red)),
                              child: const Text("Ease and continue")),
                        ],
                        title: const Text("Confirmation"),
                        content: SizedBox(
                          child: RichText(
                            text: const TextSpan(
                              children: [
                                TextSpan(
                                    text:
                                        'This action will erase previous wallet and all the funds will be lost.'),
                                TextSpan(
                                    text: ' This action is irreversible',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.red))
                              ],
                              style: TextStyle(color: Colors.black),
                            ),
                          ),
                        ));

                    showDialog(context: context, builder: (context) => alert);
                  },
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width,
                    child: const Text(
                      "setup new one",
                      style: TextStyle(
                        color: kPrimaryColor,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.underline,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 30,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
