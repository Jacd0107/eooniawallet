import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:wallet/config.dart';
import 'package:wallet/ui/onboard/wallet_setup_screen.dart';
import 'package:wallet/ui/shared/wallet_button.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class OnboardScreen extends StatefulWidget {
  static String route = "onboard_screen";
  const OnboardScreen({Key? key}) : super(key: key);

  @override
  State<OnboardScreen> createState() => _OnboardScreenState();
}

class _OnboardScreenState extends State<OnboardScreen> {
  List<PageViewModel> pageList = [
    PageViewModel(
      title: "Trusted by Million",
      body:
          "Here you can write the description of the page, to explain someting...",
      image: Center(
        child: SvgPicture.asset(
          "assets/vector/phone.svg",
          height: 200,
        ),
      ),
    ),
    PageViewModel(
      title: "Safe, Reliable and Superfast",
      body:
          "Here you can write the description of the page, to explain someting...",
      image: Center(
        child: SvgPicture.asset(
          "assets/vector/eth.svg",
          height: 200,
        ),
      ),
    ),
    PageViewModel(
      title: "Your key to explore Web3",
      body:
          "Here you can write the description of the page, to explain someting...",
      image: Center(
        child: SvgPicture.asset(
          "assets/vector/world.svg",
          height: 200,
        ),
      ),
    ),
  ];
  int index = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SizedBox(
          height: double.infinity,
          width: double.infinity,
          child: Column(
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Column(
                  children: [
                    Text(
                      appName.toUpperCase(),
                      style: const TextStyle(
                          fontWeight: FontWeight.w300,
                          fontSize: 25,
                          letterSpacing: 5),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: IntroductionScreen(
                  initialPage: index,
                  pages: pageList,
                  showNextButton: false,
                  showDoneButton: false,
                  onDone: () {
                    // When done button is press
                  },
                ),
              ),
              WalletButton(
                textContent: AppLocalizations.of(context)!.getStarted,
                // textContent: "",
                onPressed: () {
                  Navigator.of(context).pushNamed(WalletSetupScreen.route);
                },
              ),
              const SizedBox(
                height: 30,
              )
            ],
          ),
        ),
      ),
    );
  }
}
