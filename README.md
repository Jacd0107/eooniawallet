# wallet

Your key to web3

## Quickstart:
Setup and this application is super simple, All you need is two things
1. Infura key
2. Etherscan key

## 1) Infura:
Infura provides a node which is used to communicate to the blockchain. For that you
need an API key from infura.
● Getting the API key is easy just create one account on Infura then create an
application and they’ll provide you a unique API key.
● Paste that API Key in config.dart file


## 2) Etherscan:
Etherscan is used to get blockchain data from off-chain. For that you need an API key
from Etherscan.
● Getting the API key is easy just create one account on etherscan then create an
application and they’ll provide you a unique API key.
● Paste that API Key in config.dart file
After following these steps make. Open in your project folder in VS code and install
flutter plugin then run main.dart file