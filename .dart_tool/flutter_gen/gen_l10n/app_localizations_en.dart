import 'app_localizations.dart';

/// The translations for English (`en`).
class AppLocalizationsEn extends AppLocalizations {
  AppLocalizationsEn([String locale = 'en']) : super(locale);

  @override
  String get appName => 'Cryptomask';

  @override
  String get getStarted => 'Get Started';

  @override
  String get currentLanguage => 'Current language';

  @override
  String get trusedByMillion => 'Trused by Million';

  @override
  String get safeReliableSuperfast => 'Safe, Reliable and Superfast';

  @override
  String get youKeyToExploreWeb3 => 'Your key to explore Web3';

  @override
  String get template1 => 'Here you can write the description of the page,  to explain something...';

  @override
  String get template2 => 'Here you can write the description of the page,  to explain something...';

  @override
  String get template3 => 'Here you can write the description of the page,  to explain something...';

  @override
  String get walletSetup => 'Wallet setup';

  @override
  String get importAnExistingWalletOrCreate => 'Import an existing wallet or create a new one';

  @override
  String get importUsingSecretRecoveryPhrase => 'Import using Secret Recovery Phrase';

  @override
  String get createANewWallet => 'Create a new wallet';

  @override
  String get importAccount => 'Import account';

  @override
  String get secretRecoveryPhrase => 'Secret Recovery Phrase';

  @override
  String get password => 'Password';

  @override
  String get importWallet => 'Import Wallet';

  @override
  String get passPhraseNotEmpty => 'Passpharse shouldn\'t be empty';

  @override
  String get passwordNotEmpty => 'Passwored shouldn\'t be empty';

  @override
  String get enterYourSecretRecoveryPharse => 'Enter your Secret Recovery Phrase';

  @override
  String get enterNewPassword => 'Enter new password';

  @override
  String get secureWallet => 'Secure wallet';

  @override
  String get createPassword => 'Create password';

  @override
  String get confirmSeed => 'Confirm seed';

  @override
  String get thisPasswordWill => 'This password will unlock your wallet only on this device.';

  @override
  String get newPassword => 'New password';

  @override
  String get show => 'Show';

  @override
  String get confirmPassword => 'Confirm password';

  @override
  String get mustBeAtleast => 'Must be atleast 8 character';

  @override
  String get passwordMustContain => 'Password must contain atleast 8 characters';

  @override
  String iUnserstandTheRecover(String appName) {
    return 'I understand the $appName cannot recover this password for me. Learn more';
  }

  @override
  String get thisFieldNotEmpty => 'This filed shouldn\'t be empty';

  @override
  String get writeSecretRecoveryPhrase => 'Write down your Secret Recovery Phrase';

  @override
  String get yourSecretRecoveryPhrase => 'This is your Secret RecoveryPhrase. Write it down on a paper and keep it in a safe place. You\'ll be asked to re-enter this phrase (in order) on the next step';

  @override
  String get tapToReveal => 'Tap to reveal you Secret Recovery Phrase';

  @override
  String get makeSureNoOneWatching => 'Make sure no one is watching your screen';

  @override
  String get continueT => 'Continue';

  @override
  String get selectEachWord => 'Select each word in the order it was presented to you';

  @override
  String get reset => 'Reset';

  @override
  String get view => 'View';

  @override
  String get receive => 'Receive';

  @override
  String get send => 'Send';

  @override
  String get swap => 'Swap';

  @override
  String get tokens => 'Tokens';

  @override
  String get collectibles => 'Collectibles';

  @override
  String get dontSeeYouToken => 'Don\'t see your tokens?';

  @override
  String get importTokens => 'Import Tokens';

  @override
  String get scanAddressto => 'Scan adress to receive payment';

  @override
  String get copy => 'Copy';

  @override
  String get requestPayment => 'Request Payment';

  @override
  String get dontSeeYouCollectible => 'Don\'t see your NFTs?';

  @override
  String get importCollectible => 'Import NFT';

  @override
  String get importTokensLowerCase => 'Import tokens';

  @override
  String get search => 'Search';

  @override
  String get customTokens => 'Custom Token';

  @override
  String get thisFeatureInMainnet => 'This feature only available on mainnet';

  @override
  String get anyoneCanCreate => 'Anyone can create a token, including creating fake versions of existing tokens. Learn more about scams and security risks';

  @override
  String get tokenAddress => 'Token address';

  @override
  String get tokenSymbol => 'Token symbol';

  @override
  String get tokenDecimal => 'Token Decimal';

  @override
  String get cancel => 'Cancel';

  @override
  String get import => 'Import';

  @override
  String get top20Token => 'Top ERC20 token';

  @override
  String get importToken => 'Import token';

  @override
  String get tokenAddedSuccesfully => 'Token added successfully';

  @override
  String get collectibleAddedSuccesfully => 'Collectible added successfully';

  @override
  String get tokenName => 'Token name';

  @override
  String get tokenID => 'Token ID';

  @override
  String get nftOwnedSomeone => 'NFT is owned by someone, You can only import NFT that you owned';

  @override
  String get nftDeleted => 'NFT deleted successfully';

  @override
  String get youHaveNoTransaction => 'You have not transaction';

  @override
  String get from => 'From';

  @override
  String get to => 'To';

  @override
  String get searchPublicAddress => 'Search public address (0x), or ENS';

  @override
  String get transferBetweenMy => 'Transfer between my accounts';

  @override
  String get recent => 'Recent';

  @override
  String get balance => 'Balance';

  @override
  String get back => 'Back';

  @override
  String get useMax => 'Use MAX';

  @override
  String get amount => 'Amount';

  @override
  String get likelyIn30Second => 'Likely in < 30 seconds';

  @override
  String get likelyIn15Second => 'Likely in 15 seconds';

  @override
  String get mayBeIn30Second => 'Maybe in 30 seconds';

  @override
  String get estimatedGasFee => 'Estimated gas fee';

  @override
  String get total => 'Total';

  @override
  String get maxFee => 'Max fee';

  @override
  String get maxAmount => 'Max amount';

  @override
  String get transactionFailed => 'Transaction failed';

  @override
  String get transactionSubmitted => 'Transaction submitted';

  @override
  String get confirmAndApprove => 'Confirm and Approve';

  @override
  String get waitingForConfirmation => 'Waiting for confirmation';

  @override
  String get editPriority => 'Edit priority';

  @override
  String get low => 'Low';

  @override
  String get medium => 'Market';

  @override
  String get high => 'High';

  @override
  String get advanceOptions => 'Advance options';

  @override
  String get howShouldIChoose => 'How should I choose';

  @override
  String get gasLimit => 'Gas limit';

  @override
  String get maxPriorityGwei => 'Max priority fee (GWEI)';

  @override
  String get maxFeeSwei => 'Max fee (GWEI)';

  @override
  String get confirmTrasaction => 'Confirm transaction';

  @override
  String get selectTokenToSwap => 'Select Token to swap';

  @override
  String get selectaToken => 'Select a token';

  @override
  String get getQuotes => 'Get quotes';

  @override
  String get convertFrom => 'Convert from';

  @override
  String get convertTo => 'Convert to';

  @override
  String get enterTokenName => 'Enter token name';

  @override
  String get newQuoteIn => 'New quote in';

  @override
  String get availableToSwap => 'available to swap';

  @override
  String get swipeToSwap => 'Swipe to swap';

  @override
  String get wallet => 'Wallet';

  @override
  String get transactionHistory => 'Transaction History';

  @override
  String get viewOnEtherscan => 'View on Etherscan';

  @override
  String get shareMyPubliAdd => 'Share my Public Address';

  @override
  String get settings => 'Settings';

  @override
  String get getHelp => 'Get Help';

  @override
  String get logout => 'Logout';

  @override
  String get explorer => 'Explorer';

  @override
  String get general => 'General';

  @override
  String get generalDescription => 'Currency conversion, primary currency, language and search engine';

  @override
  String get networks => 'Networks';

  @override
  String get networksDescription => 'Add and edit custom RPC networks';

  @override
  String get contacts => 'Contacts';

  @override
  String get contactDescription => 'Add, edit, remove and manage you accounts';

  @override
  String about(String appName) {
    return 'About $appName';
  }

  @override
  String get currencyConversion => 'Currency conversion';

  @override
  String get displayFiat => 'Display fiat values in using a specific currency throughout the application';

  @override
  String get languageDescription => 'Translate the application to a different supported language';

  @override
  String get createNewAccount => 'Create New Account';

  @override
  String get security => 'Security';

  @override
  String get securityDescription => 'Manage privatekey and export wallet';

  @override
  String get showPrivateKey => 'Show private key';

  @override
  String get tapHereToReveal => 'Tap and hold to reveal and copy private key';

  @override
  String get exportWallet => 'Export wallet';

  @override
  String get tapHereToExportWallet => 'Tap and hold to export wallet (Your current password is used for import)';
}
